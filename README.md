## TALER MAVEN - NEXUS

Objetivo:
Configurar un repositorio para control de dependencias maven con Nexus en una imagen de docker. Los requisitos para tomar el taller deben ser conocimientos básicos de maven y java. 


### 1. Instalación de Maven
En este paso realizaremos la descarga y configuración de maven en nuestro equipo.

Descargar archivos binarios de maven del siguiente enlace (version a indicar por el instructor)  https://maven.apache.org/download.cgi

Pocisionarse en la carpeta downloads y extraer los binarios del archivo 

    tar -xvf apache-maven-3.8.6-bin.tar.gz

Mover la carpeta a la dirección **Users/Guest**

    mv apache-maven-3.8.6 /Users/Guest

El siguiente paso, será modificar el archivo .bashprofile, para esto nos ubicamos en la dirección donde se encuentra el archivo 

    cd ~

abrimos el documento **.bashprofile**

    nano .bash_profile

editamos el archivo añadiendo lo siguiente:

    export M2_HOME=/Users/Guest/apache-maven-3.8.6
    export PATH=$PATH:/Users/Guest/apache-maven-3.8.6/bin

finalizada la configuración, realizamos pruebas para validar que maven está listo para utilizarse
    mvn -version

    Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
    Maven home: /Users/Guest/apache-maven-3.8.6
    Java version: 11.0.11, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk-11.0.11.jdk/Contents/Home
    Default locale: en_MX, platform encoding: UTF-8
    OS name: "mac os x", version: "10.16", arch: "x86_64", family: "mac"

Realizar proyecto de prueba


### 2. Instalación Nexus Docker
Descargar y configurar imagen de nexus de docker hub

Acceder a la siguiente URL https://hub.docker.com/r/sonatype/nexus3

    docker pull sonatype/nexus3

creamos un volumen 
    docker volume create --name nexus-data

ejecutamos el volumen de docker en el puerto 8081

    docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3

Tomará algunos minutos iniciar el servidor 

![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen%20Shot%202022-10-20%20at%202.08.56.png)
    
Como se muestra en la imagen anterior, debemos acceder a ***nexus-data*** para obtener la contraseña.

accedemos al bash de nuestro volumen para obtener la contraseña temporal y poder iniciar sesión en nuestro repositorio

    docker exec -it nexus /bin/bash

dentro del bash de nuestro contenedor, ejecutamos el siguiente comando para poder visualizar la contraseña inicial/temporal

    cat /nexus-data/admin.password

Copiamos y pegamos nuestra contraseña , accediendo con el usuario ***admin*** y contraseña ***<hashobtenido>***


Se deplegará una modal para realizar la configuración de inicio del servidor 

![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen_Shot_2022-10-20_at_2.29.47.png)

proporcionar la contraseña ***admin123***

![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen_Shot_2022-10-20_at_2.30.03.png)


![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen_Shot_2022-10-20_at_2.30.28.png)

Damos clic en finalizar para terminar con la configuración

![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen_Shot_2022-10-20_at_2.30.41.png)



### Configuración maven (settings.xml)

Para continuar, debemos de buscar la ubicación de nuestro repositorio local y abrimos la terminal en esa ubicación 

creamos el archivo **settings.xml** y añadimos la sig configuración 



    This XML file does not appear to have any style information associated with it. The document tree is shown below.
    <settings>
        <servers>
            <server>
                <id>repo</id>
                <username>admin</username>
                <password>admin123</password>
            </server>
        </servers>
        <profiles>
            <profile>
                <id>env-jdk11</id>
                <repositories>
                    <repository>
                        <id>central</id>
                        <name>Repository for Legados JDK 1.11 builds</name>
                        <url>http://localhost:8081/repository/maven-public/</url>
                        <snapshots>
                            <enabled>false</enabled>
                        </snapshots>
                    </repository>
                    <repository>
                        <id>snapshots</id>
                        <url>http://localhost:8081/repository/maven-public/</url>
                        <releases>
                        <enabled>false</enabled>
                        </releases>
                    </repository>
                </repositories>
                <pluginRepositories>
                    <pluginRepository>
                        <id>central</id>
                        <url>http://localhost:8081/repository/maven-public/</url>
                        <snapshots>
                            <enabled>false</enabled>
                        </snapshots>
                    </pluginRepository>
                        <pluginRepository>
                        <id>snapshots</id>
                        <url>http://localhost:8081/repository/maven-public/</url>
                        <releases>
                            <enabled>false</enabled>
                        </releases>
                    </pluginRepository>
                </pluginRepositories>
            </profile>
        </profiles>
    </settings>


Para realizar pruebas sobre la configuración realizada, es necesario crear un proyecto maven y contruirlo referenciando a unos de los perfiles maven declarados



### Crear y agrupar repositorios Maven en NEXUS

Damos clic en crear repositorio

![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen_Shot_2022-10-20_at_22.22.45.png)

Seleccionamos repositorio del tipo hosted 

![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen_Shot_2022-10-20_at_22.25.14.png)

Escribimos el nombre de ***itl-realeses*** y habilitamos los redeploys
![stack Overflow](https://gitlab.com/JuanEspinoza/wiki_maven_nexus/-/raw/main/imagenes/Screen_Shot_2022-10-20_at_23.06.08.png
)


Para finalizar, seleccionamos el grupo de repositorios de ***maven-public*** y añadimos el repositorio de realeses recien creado.



### Deployar dependencia en nuestro repositorio 

El ejemplo consiste en crear una dependencia y deployarla en el repositorio remoto recien creado

Link de repositorio: [https://gitlab.com/JuanEspinoza/pokemon-client](https://gitlab.com/JuanEspinoza/pokemon-client)

    mvn deploy -Penv-jdk11  -DskipTests -Dmaven.install.skip=true

### Descarga dependencia de nuestro repositorio local 

El ejemplo consiste en crear un proyecto el cual utilice la dependencia recien deployada

Link del repositorio: [https://gitlab.com/JuanEspinoza/proyecto-maven](https://gitlab.com/JuanEspinoza/proyecto-maven)


